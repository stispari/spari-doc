# SPARI API 1.0 Documentation

## Format de l'URL pour accéder à l'API
Bienvenue sur la documentation officielle de SPARI API v1.0

les accès à l'API fonctionnent avec des variables HTML GET :
 >firewarner.pdva.eu/?variable1=55847&variable2=bonjour
 
ici, variable1 = 55847 et variable2 = bonjour

les données renvoyées sont au format JSON. [Cliquez ici](https://la-cascade.io/json-pour-les-debutants/ "Cliquez ici") si vous n'avez jamais utilisé cette notation.

## Schéma de variables

![Schéma de variables](https://i.imgur.com/YoXG18s.png)

## Fonctionnement du Schéma de variables

Imaginons que l'on souhaite récupérer un fichier de recherche, la case rouge associée est l'étape de renvoi du fichier. En suivant le schéma à l'envers, il est possible de voir les variables que l'ont doit renseigner.
Ici : 
> ?request=get&data=synthesis&pos=[renseigner position]

## Fichier de recherche

Un fichier de recherche est un fichier qui, pour des paramètres renseignés, renvoie une liste de toute les mesures ayant été faites dans ces paramètres

Exemple:

```json
{
  "search": [
    {
      "date": "20180612",
      "index": 8742,
      "position": {
        "latitude": 44.04917,
        "longitude": 2.15806
      }
    },
    {
      "date": "20180712",
      "index": 8872,
      "position": {
        "latitude": 44.05577,
        "longitude": 2.15889
      }
    }
  ]
}
```

## Fichier LOG

Le fichier LOG est un fichier réinitialisé a intervalle régulier. Il contient l'index des dernières mesures effectuées. Il est organisé par date.

reload-date est la date de création de la liste

reload-time est l'heure de création de la liste au format simple (23:55 deviendra 2355)

les objets sont classés par ordre d'apparition et contiennent l'index associé

Exemple:

```json
{
  "indexes" : [
    772,
    487,
    430
  ],
  "information" : {
    "reload-time" : 2355,
    "reload-date" : 20180812
  }
}
```

## Fichier de Synthèse
Le fichier de synthèse est le fichier possédant les informations de capture de données et les données météorologique, ainsi que le risque associé.

le fichier contient deux objets :

un objet "header" qui contient des informations sur la séance de capture :

"risk" : le risque calculé sur 10

"date" : la date de capture

"start-time" : l'heure de début de capture

"end-time" : l'heure de fin de capture

"interval" : l'intervalle de temps de mesure en seconde

"global-pos" est une position approximative de la zone capturée


un objet "data" qui contient lui même un certain nombre de données
ces données sont classées par ordre croissant et qui possède les informations sur la capture :

"position" : la position (Latitude, Longitude) de la mesure

"humidity" : valeur du capteur d'humidité

"temperature" : valeur du thermomètre


Exemple:

```json
{
  "header" : {
    "risk" : 0,
    "date" : 20181012,
    "start-time" : 2145,
    "end-time" : 2204,
    "interval" : 25,
    "global-position" : {
      "latitude" : 44.0500339,
      "longitude" : 2.1607500000000073
    }
  },
  "data" : [
    {
      "position" : {
        "latitude" : 0,
        "longitude" : 0,
        "altitude"  : 0
      },
      "humidity" : 0,
      "temperature" : 0
    },
    {
      "position" : {
        "latitude" : 0,
        "longitude" : 0,
        "altitude"  : 0
      },
      "humidity" : 0,
      "temperature" : 0
    },
    {
      "position" : {
        "latitude" : 0,
        "longitude" : 0,
        "altitude"  : 0
      },
      "humidity" : 0,
      "temperature" : 0
    },
    {
      "position" : {
        "latitude" : 0,
        "longitude" : 0,
        "altitude"  : 0
      },
      "humidity" : 0,
      "temperature" : 0
    },
    {
      "position" : {
        "latitude" : 0,
        "longitude" : 0,
        "altitude"  : 0
      },
      "humidity" : 0,
      "temperature" : 0
    }
  ]
}
```

